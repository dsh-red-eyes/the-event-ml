FROM ubuntu:20.10
WORKDIR "/home/"
EXPOSE 8080
ADD ./webserver.py .
ADD ./teach_model.py .
ADD ./event.csv .
RUN apt update; apt install --assume-yes python3.8; apt install --assume-yes python3-pip; pip3 install sklearn
RUN python3 teach_model.py
RUN apt-get autoclean
CMD python3 webserver.py
