import joblib

clf = joblib.load('event_model.pkl')

input_data = [
    ",event: football       inventory: ball|web|24",
    ",event: football       inventory: ball|web|24"
]

predicted_attacks = clf.predict(input_data)
for data, item in zip(input_data, predicted_attacks):
    print(u'\n{} ----> {}'.format(item, data))