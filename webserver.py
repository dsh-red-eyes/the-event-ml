import http.server
import json

import joblib

clf = joblib.load('event_model.pkl')
PORT = 8000


class MLHttpRequestHandler(http.server.BaseHTTPRequestHandler):
    def do_POST(self):
        self.send_response(200)
        self.send_header('content-type', 'application/json')
        self.end_headers()
        content_len = int(self.headers.get('Content-Length'))
        post_body = self.rfile.read(content_len).decode("utf-8")
        input_data = [post_body]
        predicted_attacks = clf.predict(input_data)
        output = []
        for data, item in zip(input_data, predicted_attacks):
            output.append(item)
        self.wfile.write(json.dumps(output).encode('utf-8'))


handler = MLHttpRequestHandler
service = http.server.HTTPServer(('0.0.0.0', PORT), handler)

service.serve_forever()
